from typing import List

from .wiki import Wiki


class K8sWiki(Wiki):
    def __init__(self, name: str, version: str, extensions: List[str], namespace: str, context: str) -> None:
        super().__init__(name, version, extensions)
        self.namespace = namespace
        self.context = context

    def connect(self) -> None:
        print(f"Connecting to Kubernetes cluster with context: {self.context}")

    def deploy(self) -> None:
        print(f"Deploying {self.name} to Kubernetes namespace: {self.namespace}")

    def update(self) -> None:
        print(f"Updating {self.name} in Kubernetes namespace: {self.namespace}")
