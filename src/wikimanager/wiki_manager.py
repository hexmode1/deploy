"""A manager of wikis."""

from .config import Config
from .k8s_wiki import K8sWiki
from .wiki import Wiki


class WikiManager:
    """A class for managing wikis."""

    def __init__(self, config_path: str):
        self.config_path = config_path
        self.wikis: list[Wiki] = []
        self.load_wikis()

    def load_wikis(self) -> None:
        """Load Wiki information."""
        config = Config(self.config_path)
        config.load()
        self.wikis = list(config.wiki_list())

    def save_wikis(self) -> None:
        """Save Wiki information."""
        config = {
            "wikis": [
                {
                    "name": wiki.name,
                    "type": "k8s" if isinstance(wiki, K8sWiki) else "ec2",
                    "version": wiki.version,
                    "extensions": wiki.extensions,
                }
                for wiki in self.wikis
            ]
        }
        Config.create_wiki_from_config(config)
        Config(self.config_path).save()

    def add_wiki(self, wiki: Wiki) -> None:
        """Add a wiki to those under management."""
        self.wikis.append(wiki)

    def deploy_all(self) -> None:
        """Deploy all the wikis under management."""
        for wiki in self.wikis:
            wiki.connect()
            wiki.deploy()

    def update_all(self) -> None:
        """Update all the wikis under management."""
        for wiki in self.wikis:
            wiki.connect()
            wiki.update()
