"""The configuration for our wikis."""

from typing import Any, Dict, List

import yaml

from .ec2_wiki import EC2Wiki
from .k8s_wiki import K8sWiki
from .wiki import Wiki

__all__ = ["Config"]


class Config:
    """Class for management of configuration."""

    def __init__(self, file_path: str):
        self.file_path = file_path
        self.wikis = self.load()

    def load(self) -> List[Wiki]:
        """Load configuration."""
        with open(self.file_path, "r", encoding="utf-8") as file:
            self.data = yaml.safe_load(file)

        return self.create_wikis()

    def save(self) -> None:
        """Save configuration."""
        with open(self.file_path, "w", encoding="utf-8") as file:
            yaml.dump(self.data, file)

    def create_wikis(self) -> List[Wiki]:
        """Create all wikis."""
        if isinstance(self.data, dict) and "wikis" in self.data and isinstance(self.data["wikis"], list):
            return [self.create_wiki_from_config(wiki_config) for wiki_config in self.data["wikis"]]
        return []

    def wiki_list(self) -> List[Wiki]:
        """Get a list of wikis in the config."""
        return self.wikis

    @staticmethod
    def create_wiki_from_config(wiki_config: Dict[str, Any]) -> Wiki:
        """Create a wiki from the configuration."""
        if wiki_config["type"] == "k8s":
            return K8sWiki(
                name=wiki_config["name"],
                version=wiki_config["version"],
                extensions=wiki_config["extensions"],
                namespace=wiki_config["namespace"],
                context=wiki_config["context"],
            )
        if wiki_config["type"] == "ec2":
            return EC2Wiki(
                name=wiki_config["name"],
                version=wiki_config["version"],
                extensions=wiki_config["extensions"],
                instance_id=wiki_config["instance_id"],
                region=wiki_config["region"],
                ssh_key_path=wiki_config["ssh_key_path"],
            )
        raise ValueError(f"Unknown wiki type: {wiki_config['type']}")

    def update_wiki_config(self, wiki: Wiki) -> None:
        """Update the wiki configuration."""
        for i, wiki_config in enumerate(self.data["wikis"]):
            if wiki_config["name"] == wiki.name:
                self.data["wikis"][i] = self.wiki_to_config(wiki)
                return
        self.data["wikis"].append(self.wiki_to_config(wiki))

    @staticmethod
    def wiki_to_config(wiki: Wiki) -> Dict[str, Any]:
        """Produce a configuration for a wiki."""
        config: Dict[str, Any] = {
            "name": wiki.name,
            "type": "k8s" if isinstance(wiki, K8sWiki) else "ec2",
            "version": wiki.version,
            "extensions": wiki.extensions,
        }
        if isinstance(wiki, K8sWiki):
            config.update({"namespace": wiki.namespace, "context": wiki.context})
        elif isinstance(wiki, EC2Wiki):
            config.update({"instance_id": wiki.instance_id, "region": wiki.region, "ssh_key_path": wiki.ssh_key_path})
        return config
