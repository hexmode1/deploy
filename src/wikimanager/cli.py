"""CLI for wikimanger."""

from argparse import ArgumentParser

from .ec2_wiki import EC2Wiki
from .k8s_wiki import K8sWiki
from .wiki import Wiki
from .wiki_manager import WikiManager


class Cli:
    """Class for CLI."""

    def __init__(self) -> None:
        """Initialize."""
        self.parser = self.create_parser()
        self.args = self.parser.parse_args()
        self.manager = WikiManager(self.args.config)

    def create_parser(self) -> ArgumentParser:
        """Set up the parser."""
        parser = ArgumentParser(description="Manage wikis on remote hosts")
        parser.add_argument("--config", default="config.yaml", help="Path to the configuration file")

        subparsers = parser.add_subparsers(dest="command", help="Sub-command help")

        # List command
        subparsers.add_parser("list", help="List all wikis")

        # Deploy command
        deploy_parser = subparsers.add_parser("deploy", help="Deploy all wikis")
        deploy_parser.add_argument("--wiki", help="Deploy a specific wiki by name")

        # Update command
        update_parser = subparsers.add_parser("update", help="Update all wikis")
        update_parser.add_argument("--wiki", help="Update a specific wiki by name")

        # Add command
        add_parser = subparsers.add_parser("add", help="Add a new wiki")
        add_parser.add_argument("--name", required=True, help="Name of the wiki")
        add_parser.add_argument("--type", choices=["k8s", "ec2"], required=True, help="Type of wiki")
        add_parser.add_argument("--version", required=True, help="MediaWiki version")
        add_parser.add_argument("--extensions", nargs="+", default=[], help="List of extensions")
        add_parser.add_argument("--namespace", help="Kubernetes namespace (for k8s wikis)")
        add_parser.add_argument("--context", help="Kubernetes context (for k8s wikis)")
        add_parser.add_argument("--instance-id", help="EC2 instance ID (for EC2 wikis)")
        add_parser.add_argument("--region", help="AWS region (for EC2 wikis)")
        add_parser.add_argument("--ssh-key-path", help="Path to SSH key (for EC2 wikis)")

        return parser

    def handle_list(self) -> None:
        """Handle list subcommand."""
        for wiki in self.manager.wikis:
            print(f"{wiki.name} ({wiki.__class__.__name__}): version {wiki.version}")

    def handle_deploy(self) -> None:
        """Handle deploy subcommand."""
        if self.args.wiki:
            wiki = next((w for w in self.manager.wikis if w.name == self.args.wiki), None)
            if wiki:
                wiki.deploy()
            else:
                print(f"Wiki '{self.args.wiki!r}' not found.")
        else:
            self.manager.deploy_all()

    def handle_update(self) -> None:
        """Handle update subcommand."""
        if self.args.wiki:
            wiki = next((w for w in self.manager.wikis if w.name == self.args.wiki), None)
            if wiki:
                wiki.update()
            else:
                print(f"Wiki '{self.args.wiki!r}' not found.")
        else:
            self.manager.update_all()

    def handle_add(self) -> None:
        """Handle add subcommand."""
        wiki: Wiki
        if self.args.type == "k8s":
            wiki = K8sWiki(
                self.args.name, self.args.version, self.args.extensions, self.args.namespace, self.args.context
            )
        elif self.args.type == "ec2":
            wiki = EC2Wiki(
                self.args.name,
                self.args.version,
                self.args.extensions,
                self.args.instance_id,
                self.args.region,
                self.args.ssh_key_path,
            )

        self.manager.add_wiki(wiki)
        self.manager.save_wikis()
        print(f"Added new wiki: {self.args.name}")

    def handle_usage(self) -> None:
        """Handle showing the usage message."""
        self.parser.print_usage()

    def run(self) -> None:
        """Run the CLI."""
        if self.args.command == "deploy":
            return self.handle_deploy()

        if self.args.command == "list":
            return self.handle_list()

        if self.args.command == "update":
            return self.handle_update()

        if self.args.command == "add":
            return self.handle_add()

        return self.parser.print_usage()

    @staticmethod
    def main() -> None:
        """Handle the command."""
        cli = Cli()
        cli.run()


if __name__ == "__main__":
    Cli.main()
