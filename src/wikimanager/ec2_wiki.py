"""Handle wikis on EC2."""

from typing import List

from .wiki import Wiki


class EC2Wiki(Wiki):
    """Class for managing EC2-hosted wikis."""

    def __init__(
        self, name: str, version: str, extensions: List[str], instance_id: str, region: str, ssh_key_path: str
    ):
        super().__init__(name, version, extensions)
        self.instance_id = instance_id
        self.region = region
        self.ssh_key_path = ssh_key_path

    def connect(self) -> None:
        print(f"Connecting to EC2 instance: {self.instance_id}")

    def deploy(self) -> None:
        print(f"Deploying {self.name} to EC2 instance: {self.instance_id}")

    def update(self) -> None:
        print(f"Updating {self.name} in EC2 instance: {self.instance_id}")
