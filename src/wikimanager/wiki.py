"""An abstraction of a wiki."""

from abc import ABC, abstractmethod
from typing import List


class Wiki(ABC):
    """Set up all types of wikis."""

    def __init__(self, name: str, version: str, extensions: List[str]):
        self.name = name
        self.version = version
        self.extensions = extensions

    @abstractmethod
    def connect(self) -> None:
        """Connect to the wiki."""

    @abstractmethod
    def deploy(self) -> None:
        """Deploy the wiki."""

    @abstractmethod
    def update(self) -> None:
        """Update the wiki."""
