#!/bin/bash -e

export MW_POD_NAME=${MW_POD_NAME:-}
export MW_VER=${MW_VER:-15.1.3}
export NS=${NS:-mediawiki}
export INSTANCE=${INSTANCE:-mediawiki}
export INSTANCE_HOST=${INSTANCE_HOST:-wiki.example.com}
export MEDIAWIKI_DOMAIN=${MEDIAWIKI_DOMAIN:-$INSTANCE_HOST}
export MW_DATA=${MW_DATA:-/bitnami/mediawiki}
export MW_INSTALL_PATH=${MW_INSTALL_PATH:-/opt$MW_DATA}
export REGISTRY=${REGISTRY:-oci://registry-1.docker.io/bitnamicharts/mediawiki}
export EXTENSION_JSON_FILE="ExtensionJson.json"
export CONFIG_MAP_NAME=${CONFIG_MAP_NAME:-mediawiki-composer-local}
export LUA_VER=${LUA_VER:-5.1.5}
export LUA_REPO=https://www.lua.org/ftp/
export INSTANCE_PASSWORD
export SECRET_NAME

max_retries=5
delay_between_retries=10 # seconds

# Function to perform the retry logic
retry_command() {
    local n=0

    until [ "$n" -ge "$max_retries" ]
    do
        set +e
        command "$@"
        ret=$?
        if [[ $ret -eq 0 || $ret -eq 1 ]]; then
            set -e
            return "$ret"
        fi
        set -e
        WARNING "Command failed, will retry in $delay_between_retries seconds..."
        n=$((n+1))
        sleep "$delay_between_retries"
        DEBUG "Attempt $((n+1)) of $max_retries"
    done
    WARNING "Command has failed after $n attempts."
    return 100
}

kubectl() {
    ret=0
    DEBUG "kubectl $*"
    if [[ $max_retries -eq 0 ]]; then
        command kubectl "$@"
        ret=$?
    else
        retry_command kubectl "$@"
        ret=$?
    fi
    # DEBUG "kubectl returning $ret"
    return "$ret"
}

helm() {
    ret=0
    DEBUG "helm $*"
    if [[ $max_retries -eq 0 ]]; then
        command helm "$@"
        ret=$?
    else
        retry_command helm "$@"
        ret=$?
    fi
    # DEBUG "helm returning $ret"
    return "$ret"
}

DEBUG() {
    local msg="$*"

    echo "🐛 $msg" >&2
}

WARNING() {
    local msg="$*"

    echo "⚡ $msg" >&2
}

INFO() {
    local msg="$*"

    echo "ℹ $msg" >&2
}

DIE() {
    local msg=$1
    local exitcode=${2:-10}

    echo "💀 $msg" >&2
    exit "$exitcode"
}

deploy_mw() {
    DEBUG "${FUNCNAME[0]}"
    local ret
    local values

    get_values_arg values

    # shellcheck disable=SC2086  # We want IFS on $values
    helm install -n "$NS" "$INSTANCE" "$REGISTRY" --version "$MW_VER" $values
    # This rollout status doesn't work, so we'll comment it out
    #kubectl rollout status "deployment/$INSTANCE" -n "$NS"
}

get_secret_name() {
    DEBUG "${FUNCNAME[0]} $*"
    local secret_name=$1
    local -n var_name=$2

    var_name=""
    while [ -z "$var_name" ]; do
        json=$(kubectl get secret -n "$NS" \
                       -l app.kubernetes.io/instance="$INSTANCE" \
                       -l app.kubernetes.io/name="$secret_name" \
                       -o json)
        var_name=$(jq -r '.items[0].metadata.name // ""'<<<"$json")
        if [[ -z "$ret" ]]; then
            sleep 3
        fi
    done

    DEBUG "${FUNCNAME[0]} → got secret_name='$var_name'"
}

get_secret() {
    DEBUG "${FUNCNAME[0]} $*"
    local -n ret=$1
    local name=$1
    local path=$2
    local type=${3:-mediawiki}

    while [ -z "$ret" ]; do
        get_secret_name "$type" SECRET_NAME

        # Check to see if the value was retrieved successfully and is not empty
        if ! ret=$(kubectl get secret --namespace "$NS" "$SECRET_NAME" \
                           -o jsonpath="$path" | base64 -d) || [ -z "$ret" ]; then
            WARNING "Error retrieving secret $name"
            sleep 3
        fi
    done

    DEBUG "${FUNCNAME[0]} → got $name from $path"
}

get_service() {
    DEBUG "${FUNCNAME[0]} $*"
    local -n ret=$1
    local name=$1
    local template=$2

    ret=$(kubectl get svc --namespace "$NS" "$INSTANCE" --template "$template")
    DEBUG "${FUNCNAME[0]} → got '$ret' for '$name' from '$template'"
}

get_instance_password() {
    if [[ -n "$INSTANCE_PASSWORD" ]]; then
        return
    fi
    DEBUG "${FUNCNAME[0]}"

    get_secret INSTANCE_PASSWORD "{.data.mediawiki-password}"
}

get_mariadb_root_password() {
    if [[ -n "$MARIADB_ROOT_PASSWORD" ]]; then
        return
    fi
    DEBUG "${FUNCNAME[0]}"

    get_secret MARIADB_ROOT_PASSWORD "{.data.mariadb-root-password}" mariadb
}

get_mariadb_password() {
    if [[ -n "$MARIADB_PASSWORD" ]]; then
        return
    fi
    DEBUG "${FUNCNAME[0]}"

    get_secret MARIADB_PASSWORD "{.data.mariadb-password}" mariadb
}

get_instance_host() {
    if [[ -n "$INSTANCE_HOST" ]]; then
        return
    fi
    DEBUG "${FUNCNAME[0]}"

    get_service INSTANCE_HOST "{{ range (index .status.loadBalancer.ingress 0) }}{{ . }}{{ end }}"
}

get_mw_pod_name() {
    local name=$1
    local blob
    if [[ -z $name ]]; then
        name="MW_POD_NAME"
    fi
    local -n ret=$name

    if [[ -z $ret ]]; then
        blob=$(kubectl get pods -n "$NS" -l app.kubernetes.io/name=mediawiki -o json 2> /dev/null)
        ret=$(jq -r 'first(.items[] | select(.metadata.name) | .metadata.name) // ""' <<<"$blob")
        if [[ -z $ret ]]; then
            DIE "No pod found!: $blob"
        fi
    fi
}

get_values_arg() {
    local -n ret=$1

    if [ -f values.yaml ]; then
        ret="--values values.yaml"
    fi

    if [ -f values.json ]; then
        ret="--values values.json"
    fi
}

save_values() {
    cat<<-EOF > values.yaml
	---
	ingress:
	  annotations:
	    kubernetes.io/ingress.class: nginx
	  enabled: true
	  hostname: $INSTANCE_HOST
	mariadb:
	  auth:
	    password: $MARIADB_PASSWORD
	    rootPassword: $MARIADB_ROOT_PASSWORD
	mediawikiHost: $INSTANCE_HOST
	mediawikiPassword: $INSTANCE_PASSWORD
	EOF
}

upgrade_mw() {
    DEBUG "${FUNCNAME[0]}"
    local values

    get_values_arg values

    if [[ -z $values ]]; then
        get_instance_host
        get_instance_password
        get_mariadb_password
        get_mariadb_root_password
        save_values
        values="--values values.yaml"
    fi
    # shellcheck disable=SC2086  # We want IFS on $values
    helm upgrade --namespace "$NS" --version "$MW_VER" "$INSTANCE" "$REGISTRY" $values
    kubectl rollout status "deployment/$INSTANCE" -n "$NS"
}

copy_file_to() {
    DEBUG "${FUNCNAME[0]} $*"
    local file=$1
    local ns=$2
    local pod=$3
    local dest=${4:-$MW_DATA/$(basename "$file")}

    get_mw_pod_name pod
    kubectl cp -n "$ns" "$file" "$pod:$dest"
}

copy_file_from() {
    DEBUG "${FUNCNAME[0]} $*"
    local ns=$1
    local pod=$2
    local file=$3
    local dest=${4:-$(basename "$file")}

    get_mw_pod_name pod
    kubectl cp -n "$ns" "$pod:$file" "$dest"
}

extract_dist() {
    DEBUG "${FUNCNAME[0]} $*"
    local file=$1
    local pod

    get_mw_pod_name pod
    kubectl exec "$pod" -n "$NS" -- \
            sh -c "tar -C $MW_DATA -xzf $MW_DATA/$file LocalSettings.d images
                       im extensions skins composer.local.json"
}

get_loader() {
    # shellcheck disable=SC2016  # We want to use literal $ for this php snippet
    echo 'foreach ( glob( __DIR__ . "/LocalSettings.d/*.php" ) as $filename ) { require_once( $filename ); }'
}

append_loader() {
    DEBUG "${FUNCNAME[0]} $*"
    local file=$1
    local loader
    local pod
    loader=$(get_loader)

    get_mw_pod_name pod
    kubectl exec "$pod" -n "$NS" -- \
            sh -c "echo '$loader' | tee -a $MW_DATA/$file > /dev/null"
}

composer_update() {
    DEBUG "${FUNCNAME[0]} $*"
    local pod

    get_mw_pod_name pod
    kubectl exec "$pod" -n "$NS" -- \
            sh -c "cp $MW_DATA/composer.local.json $MW_INSTALL_PATH &&
                   cd $MW_INSTALL_PATH && composer update"
}

get_extension_list() {
    DEBUG "${FUNCNAME[0]} $*"
    local pod=$1
    local -n extension_list=$2
    local extension_dir="$MW_INSTALL_PATH/extensions"

    # shellcheck disable=SC2034  # extension_list is pass-by-ref
    extension_list=$(kubectl exec "$pod" -n "$NS" -- \
                             sh -c "find -L $extension_dir -maxdepth 2 -type f \
                                          -name extension.json |
                                    sed 's,$extension_dir/,,; s,/extension.json,,'")
}

get_deployed_chart() {
    DEBUG "${FUNCNAME[0]} $*"
    local -n ret=$1
    local deploy
    local count

    deploy="$(helm list -n "$NS" -o json)"
    if [[ -n $deploy ]]; then
        count="$(jq length <<< "$deploy")"
    fi
    if [[ $count -eq 1 ]]; then
        ret="$(jq -r '.[] | .chart' <<< "$deploy")"
    fi
}

deploy_or_upgrade() {
    DEBUG "${FUNCNAME[0]} $*"
    local -n ret=$1
    local chart

    get_deployed_chart chart
    if [[ -n "$chart" ]]; then
        upgrade_mw
        ret=upgraded
    else
        DEBUG "Installing ($chart)"
        helm install -n "$NS" --version "$MW_VER" "$INSTANCE" "$REGISTRY" \
             --set mediawikiHost="$INSTANCE_HOST"
        ret=install
    fi
}

get_missing_extensions() {
    local old_list=$1
    local new_list=$2
    local -n missing=$3
    local ext_list=()

    for extension in $old_list; do
        # Skip if we're ignoring this extension
        if grep -qw "$extension" <<<"$IGNORE_EXTENSIONS"; then
            continue
        fi

        # If this extension isn't present on the new wiki host, then get it
        if ! grep -qw "$extension" <<<"$new_list"; then
            ext_list+=("$extension")
        fi
    done
    # shellcheck disable=SC2124  # missing is pass-by-ref
    missing="${ext_list[@]}"
    DEBUG "${FUNCNAME[0]} → $missing"
}

get_current_version() {
    DEBUG "${FUNCNAME[0]} $*"
    local -n ret=$1
    local deployed

    get_deployed_chart deployed
    ret=${deployed##*-}
}

get_transformed_name() {
    local -n ret=$1

    case $ret in
        "ReplaceText")
            ret="Replace_Text"
            ;;
        "cldr")
            ret="CLDR"
            ;;
        "OpenIDConnect")
            ret="OpenID_Connect"
            ;;
    esac
}

get_extension_info() {
    DEBUG "${FUNCNAME[0]} $*"
    local name=$1
    local -n ret=$2

    local extension_tool_forge="https://extjsonuploader.toolforge.org"
    local extension_json_url="$extension_tool_forge/$EXTENSION_JSON_FILE"
    local extension_json_file="ExtensionJson.json"
    local yesterday

    yesterday=$(date -d "yesterday" "+%s")

    if [[ ! -f $extension_json_file ]]; then
        last_write=$(date -d "2 days ago" "+%s")
    else
        last_write=$(date -r "$extension_json_file" "+%s")
    fi

    if [[ $yesterday -gt $last_write ]]; then
        INFO "Updating $extension_json_file from $extension_json_url..."
        curl -s -o "$extension_json_file" "$extension_json_url" ||
            DIE "Problem getting $extension_json_file" 2
    fi

    if [[ ! -f "$extension_json_file" ]]; then
        DIE "Problem getting $extension_json_file"
    fi

    get_transformed_name name
    ret=$(jq -r --arg name "$name" '.[$name]' "$extension_json_file")
}

get_extension_url() {
    DEBUG "${FUNCNAME[0]} $*"
    local name=$1
    local -n ret=$2

    get_extension_info "$name" extension_info

    if [[ -z $extension_info || $extension_info == "null" ]]; then
        DIE "Couldn't find information about $name! $extension_info"
    fi

    ret=$(jq -r '.repository' <<<"$extension_info")

    DEBUG "Got url for ${name}: $url"
}

clone_extension() {
    DEBUG "${FUNCNAME[0]} $*"
    local name=$1
    local url=$2
    local branch=$3

    if [[ ! -e "extensions/$name" ]]; then
        git clone -b "$branch" --recurse "$url" "extensions/$name"
    else
        if [[ ! -e "extensions/$name/.git" ]]; then
            DIE "The extension $name directory should be cleaned up: not git checkout."
        fi
        pushd "extensions/$name"
        if [[ $(git remote get-url origin) != "$url" ]]; then
            DIE "The extension $name directory should be cleaned up: wrong origin."
        fi
        popd
    fi
}

publish_extension() {
    DEBUG "${FUNCNAME[0]} $*"
    local name=$1
    local pod=$2
    local extension_dir="$MW_INSTALL_PATH/extensions"

    kubectl cp -n "$NS" "extensions/$name" "$pod:$extension_dir/$name"
}

get_extension_branch() {
    local extension=$1
    local default=$2

    case $extension in
        "SimpleBatchUpload")
            echo "2.0.1"
            ;;
        "PDFEmbed")
            echo "master"
            ;;
        *)
            echo "$default"
            ;;
    esac
}

install_extension() {
    DEBUG "${FUNCNAME[0]} $*"
}


download_lua() {
    DEBUG "${FUNCNAME[0]} $*"
    local build_dir=$1
    local -n ret=$2

    ret="$build_dir/lua.tgz"
    curl -o "$ret" "$LUA_REPO/lua-$LUA_VER.tar.gz"
}

extract_lua() {
    DEBUG "${FUNCNAME[0]} $*"
    local build_dir=$1
    local tarfile=$2
    local -n ret=$3

    ret="$build_dir/lua"
    git init "$ret"
    tar -C "$build_dir/lua" --strip-components=1 -xzf "$tarfile"
    (cd "$ret" && git add . && git commit -m "Lua $LUA_VER")
}

patch_lua() {
    DEBUG "${FUNCNAME[0]} $*"
    local patch_dir=$1
    local extract_dir=$2

    (cd "$extract_dir" && git am "$patch_dir/"*.patch)
}

build_lua() {
    DEBUG "${FUNCNAME[0]} $*"
    local extract_dir=$1

    (cd "$extract_dir" && make linux)
}

copy_lua_to_pod() {
    DEBUG "${FUNCNAME[0]} $*"
    local extract_dir=$1
    local pod=$2

    copy_file_to "$extract_dir/src/lua" "$NS" "$pod" "$MW_DATA/lua"
    exec_pod chmod +x "$MW_DATA/lua"
}

fixup_lua_in_scribunto() {
    DEBUG "${FUNCNAME[0]}"
    local pod
    local tarfile
    local extract_dir
    local build_dir
    build_dir=$(mktemp -d -t utils.sh.XXXXXX)
    trap 'rm -rf $build_dir' 0

    get_mw_pod_name pod
    download_lua "$build_dir" tarfile
    extract_lua "$build_dir" "$tarfile" extract_dir
    patch_lua "$(pwd)/patch/lua" "$extract_dir"
    build_lua "$extract_dir"
    $extract_dir/src/lua -v
    copy_lua_to_pod "$extract_dir" "$pod"
}

dir_exists_on_pod() {
    DEBUG "${FUNCNAME[0]} $*"
    local ns=$1
    local pod=$2

    # Check if the file or directory exists
    local file_path=$3
    kubectl exec "$pod" -n "$ns" -- \
            test -d "$file_path"
    return $?
}

exec_pod() {
    DEBUG "${FUNCNAME[0]} $*"
    local cmd="$*"
    local pod_name

    get_mw_pod_name pod_name
    kubectl exec "$pod_name" -n "$NS" -- \
            sh -c "$cmd"
}

mw_run() {
    DEBUG "${FUNCNAME[0]} $*"

    exec_pod "php $MW_INSTALL_PATH/maintenance/run.php $*"
    return $?
}

has_been_appended() {
    DEBUG "${FUNCNAME[0]} $*"
    local file=$1
    local loader
    loader=$(get_loader)

    exec_pod "test -f '$MW_DATA/$file' && grep -q 'LocalSettings.d' '$MW_DATA/$file'"
    return $?
}

test_localsettings_dir() {
    DEBUG "${FUNCNAME[0]}"

    if ! has_been_appended LocalSettings.php; then
        exec_pod "cp $MW_DATA/LocalSettings.php $MW_DATA/tempLS.php"
        append_loader tempLS.php

        mw_run "--conf=$MW_DATA/tempLS.php update --quick"

        INFO "tampLS.php worked... copying to LocalSettings.php"
        exec_pod "cp $MW_DATA/tempLS.php $MW_DATA/LocalSettings.php"
    fi
}

get_sub_dirs() {
    echo a b c d e f 0 1 2 3 4 5 6 7 8 9 archive deleted thumb transcoded
}

export_files() {
    DEBUG "${FUNCNAME[0]} $*"
    local ns=$1
    local from_pod

    NS=$ns get_mw_pod_name from_pod

    mkdir -p images
    for sub in $(get_sub_dirs); do
        if [[ ! -d "images/$sub" ]]; then
            copy_file_from "$ns" "$from_pod" "$MW_DATA/images/$sub" "images/$sub"
        fi
    done
}

import_files() {
    DEBUG "${FUNCNAME[0]} $*"
    local ns=$1
    local to_pod

    NS=$ns get_mw_pod_name to_pod

    mkdir -p images
    for sub in $(get_sub_dirs); do
        if ! dir_exists_on_pod "$ns" "$to_pod" "$MW_DATA/images/$sub"; then
            copy_file_to "images/$sub" "$ns" "$to_pod" "$MW_DATA/images/$sub"
        fi
    done
}

export_sql() {
    DEBUG "${FUNCNAME[0]} $*"
    local ns=$1
    local from_pod
    local file=dump.sql.gz

    if [[ ! -f dump.sql.gz ]]; then
       NS=$ns get_mw_pod_name from_pod

       MARIADB_ROOT_PASSWORD=
       NS=$ns get_mariadb_root_password
       # shellcheck disable=SC2016  # We want pod-side envvars
       NS=$ns MW_POD_NAME=$from_pod exec_pod "'mysqldump -uroot -p'$MARIADB_ROOT_PASSWORD' \
                                                        -h"'$MARIADB_HOST bitnami_mediawiki |
                                                        gzip > '"$MW_DATA/$file'"
       copy_file_from "$ns" "$from_pod" "$MW_DATA/$file" "$file"
    fi
}

import_sql() {
    DEBUG "${FUNCNAME[0]} $*"
    local ns=$1
    local to_pod

    NS=$ns get_mw_pod_name to_pod

    MARIADB_ROOT_PASSWORD=
    NS=$ns get_mariadb_root_password
    copy_file_to dump.sql.gz "$ns" "$to_pod" "$MW_DATA/dump.sql.gz"
    # shellcheck disable=SC2016  # We want pod-side envvars
    NS=$ns exec_pod "zcat '$MW_DATA/dump.sql.gz' | mysql -uroot -p'$MARIADB_ROOT_PASSWORD'"' \
                                                   -h"$MARIADB_HOST" bitnami_mediawiki'
}

copy_localsettings_dir() {
    DEBUG "${FUNCNAME[0]} $*"
    local old_ns=$1
    local new_ns=$2
    local from_pod
    local to_pod

    NS=$old_ns get_mw_pod_name from_pod
    NS=$new_ns get_mw_pod_name to_pod

    if [[ -z $from_pod || -z $to_pod ]]; then
        DIE "Couldn't get a pod. OLD: '$from_pod' NEW: '$to_pod'"
    fi

    if [[ ! -d LocalSettings.d ]]; then
        copy_file_from "$old_ns" "$from_pod" "$MW_DATA/LocalSettings.d" LocalSettings.d
    fi
    if ! dir_exists_on_pod "$new_ns" "$to_pod" "$MW_DATA/LocalSettings.d"; then
        copy_file_to LocalSettings.d "$new_ns" "$to_pod" "$MW_DATA"
    fi
}

json_files_are_same() {
    file1=$1
    file2=$2

    diff -q <(jq -S . "$file1") <(jq -S . "$file2") > /dev/null 2>&1
    return $?
}

json_has_require() {
    local json_file=$1

    jq -e '.require and (.require | length > 0)' "$json_file" >/dev/null 2>&1
    return $?
}

json_get_require() {
    local json_file=$1
    local -n ret=$2

    ret="$(jq -r '.require // ""' "$json_file")"
}

update_composer_local_json() {
    local file=$1
    local json_data
    local required
    json_data=$(cat "$file")

    # Find all composer.json files under extensions/*/ that have a requirement and add them to the
    # JSON object iff they are not already in the list
    while IFS= read -r -d '' ext_compose; do
        DEBUG "Handling $ext_compose"
        json_get_require "$ext_compose" required
        if [[ -n "$required" ]]; then
            json_data=$(jq --argjson required "$required" '
                                .require |= . + $required
                             ' <<<"$json_data")
        fi
    done < <(find extensions -maxdepth 2 -type f -name composer.json -print0)

    # Write the JSON data to "$file"
    echo "$json_data" > "$file"
}

get_old_configmap() {
    local from_ns=$1
    local configmap=$2

    # Get current configmap, if it exists
    if [[ ! -f $file ]]; then
        (
            kubectl get -n "$from_ns" configmap "$configmap" \
                    -o jsonpath='{.data.composer\.local\.json}' 2>/dev/null || true
        ) > "$file"
    fi
}

create_composer_configmap() {
    local to_ns=$1
    local configmap=$2
    local file=composer.local.json

    # Make sure we have something json like
    if ! output=$(jq . < "$file" 2>&1); then
        DIE "Error parsing $file:\n$output"
    fi

    # Populate it if it is empty
    if [[ ! -s "$file" ]]; then
        echo {} > "$file"
    fi

    cp "$file" "$file.orig"
    update_composer_local_json "$file"
    if ! json_files_are_same "$file" "$file.orig"; then
        INFO "Updated $file"
    fi

    kubectl create -n "$to_ns" configmap "$configmap" \
            --from-file=composer.local.json="$file" \
            --dry-run=client -o yaml | kubectl apply -f -
}

get_main_volume() {
    local -n name=$1
    # get the pvc that already exists on the chart
    name=$(kubectl get pvc -n "$NS" \
                   -l app.kubernetes.io/instance="$INSTANCE" \
                   -l app.kubernetes.io/name=mediawiki \
                   -o jsonpath='{.items[0].metadata.name}')
}

add_configmap_to_extravolumes() {
    local yaml_file=$1
    local name=$2
    local config_map=$3
    local yaml_data
    local old_data
    yaml_data=$(cat "$yaml_file")
    old_data="$yaml_data"

    # shellcheck disable=SC2016  # Yes, we don't want $name and $config_map to expand
    # Check if extraVolumes entry already exists, add it if not
    yaml_data=$(yq -y --arg name "$name" --arg config_map "$config_map" '
      .extraVolumes |= if . then
                           map( if .name == $name then . else . end )
                       else
                           []
                       end |
                       if .extraVolumes | map(.name) | index($name) then
                           .
                       else
                           .extraVolumes += [{"name": $name, "configMap": {"name": $config_map}}]
                       end
        '<<<"$yaml_data")
    if [[ $yaml_data != "$old_data" ]]; then
       DEBUG "The 'extraVolumes' entry for '$name' added."
       echo "$yaml_data" > "$yaml_file"
    fi
}

add_path_to_extravolumemounts() {
    local yaml_file=$1
    local name=$2
    local mount_path=$3
    local sub_path=$4
    local yaml_data
    local old_data
    yaml_data=$(cat "$yaml_file")
    old_data="$yaml_data"

    # shellcheck disable=SC2016  # Yes, we don't want $name, etc to expand
    yaml_data=$(yq -y --arg name "$name" --arg path "$mount_path" --arg sub_path "$sub_path" '
        .extraVolumeMounts |= if . then
                                  map( if .name == $name then . else . end )
                              else
                                  []
                              end |
                              if .extraVolumeMounts | map(.name) | index($name) then
                                  .
                              else
                                  .extraVolumeMounts += [{"name": $name, "mountPath": $path, "subPath": $sub_path }]
                              end
        '<<<"$yaml_data")
    if [[ $yaml_data != "$old_data" ]]; then
       DEBUG "The 'extraVolumeMounts' entry for '$name' added."
       echo "$yaml_data" > "$yaml_file"
    fi
}

get_values() {
    local file=$1

    if [[ ! -f "$file" ]]; then
        helm get values "$INSTANCE" -n "$NS" -o yaml > "$file"
    fi
}

ensure_vendor() {
    DEBUG "${FUNCNAME[0]} $*"
    local pod=$1

    get_mw_pod_name pod
    kubectl exec "$pod" -n "$NS" -- sh -c "tar -C /opt/$MW_DATA -c vendor |
                                               tar -C $MW_DATA -x"
}

add_git_to_image() {
    DEBUG "${FUNCNAME[0]} $*"
    local base_image=$1
    local new_image="mediawiki-with-git:latest"



    if [[ -z $(docker images -q "$new_image") ]]; then
       cat <<-EOF | docker build -t "$new_image" -f - .
		FROM $base_image
		USER root
		COPY entrypoint.sh /usr/local/bin/entrypoint.sh
		RUN apt-get update && \
		    apt-get install -y git gosu && \
		    rm -rf /var/lib/apt/lists/* && \
		    touch /opt$MW_DATA/composer.local.json && \
		    touch /opt$MW_DATA/composer.lock && \
		    chmod +x /usr/local/bin/entrypoint.sh
		USER 1001
		ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
		EOF
    fi

    echo "$new_image"
}

run_composer_update() {
    DEBUG "${FUNCNAME[0]} $*"
    local composer=$1
    local comp_lock=$2
    local vendor=$3
    local image=$4
    local comp_cache
    comp_cache=$(pwd)/composer-cache

    mkdir -p "$comp_cache"
    docker run --rm \
           -u 1000:1000 \
           -v /etc/ssl/certs:/etc/ssl/certs \
           -v "$comp_cache:/.composer" \
           -v "$comp_lock:/opt$MW_DATA/composer.lock" \
           -v "$composer:/opt$MW_DATA/composer.local.json" \
           -v "$vendor:/opt$MW_DATA/vendor" \
           -w "/opt$MW_DATA" "$image" composer update
}

container_composer_update() {
    DEBUG "${FUNCNAME[0]} $*"
    local composer=$1
    local lock=$2
    local vendor=$3
    local mw_image
    local git_img

    mw_image="$(kubectl get pod -l app.kubernetes.io/name=mediawiki \
                       -o jsonpath="{.items[0].spec.containers[0].image}")"

    git_img="$(add_git_to_image "$mw_image")"
    run_composer_update "$composer" "$lock" "$vendor" "$git_img"
}

create_vendor() {
    DEBUG "${FUNCNAME[0]}"
    local pod

    get_mw_pod_name pod
    ensure_vendor "$pod"
    copy_file_from "$NS" "$pod" "/opt$MW_DATA/vendor/" vendor/
    create_composer_configmap "$NS" "$CONFIG_MAP_NAME"
    container_composer_update "$(pwd)/composer.local.json" "$(pwd)/vendor"
    copy_file_to vendor/ "$NS" "$pod" "$MW_DATA/vendor/"
}
