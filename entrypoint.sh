#!/bin/bash
set -e

# Switch to UID 1001 inside the container, but run as UID 1000
if [ "$(id -u)" = '0' ]; then
    exec gosu 1001 "$@"
fi

exec "$@"
